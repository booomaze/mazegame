import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by qpen546 on 21/04/4017.
 */
public class Map extends JPanel implements ActionListener, KeyListener {
    private Timer timer;
    private Player player;
    private Monster[] monster;
    private Movement predication;
    private String gameStatus;
    int width, height, count, life;
    Image small, heart, wall,road;
    MediaPlayer backgroundMusic;

    public Map(int w, int h) {
        monster = new Monster[4];
        predication = new Movement();
        gameStatus = "Start";
        this.setPreferredSize(new Dimension(w, h));
        init();
        width = w;
        height = h;
        count = 1;
        loadImage();
        this.addKeyListener(this);
        this.setFocusable(true);
        this.timer = new Timer(160, this);


    }
    public void simplePlayer(){

        try{
            new javafx.embed.swing.JFXPanel();
            String uriString = new File("src/image/music.mp3").toURI().toString();
           backgroundMusic= new MediaPlayer(new Media(uriString));
        }
        catch(Exception ex)
        {
            ex.printStackTrace();}
    }


    private void loadImage() {
        try {
            BufferedImage image = ImageIO.read(new File("src/image/images.jpg"));
            BufferedImage image2 = ImageIO.read(new File("src/image/heart.jpg"));
            BufferedImage image3 = ImageIO.read(new File("src/image/wall.jpg"));

            heart = image2.getScaledInstance(25, 20, image.SCALE_DEFAULT);
            small = image.getScaledInstance(20, 20, image.SCALE_DEFAULT);
            wall = image3.getScaledInstance(20, 20, image.SCALE_DEFAULT);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private int[][] maze =
            {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1}, // start position at [1,1]
                    {1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                    {1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1},
                    {1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1},
                    {1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1},
                    {1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                    {1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1},
                    {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1},
                    {1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1},
                    {1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 9, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1},//target position
                    {1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1},
                    {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1},
                    {1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1},
                    {1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1},
                    {1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1},
                    {1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1},
                    {1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1},
                    {1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1},
                    {1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1},
                    {1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1},
                    {1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1},
                    {1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1},
                    {1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};


    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        switch (gameStatus) {
            case "Start":
                showStartScreen(graphics);
                break;
            case "Introduction":
                showIntro(graphics);
                break;
            case "Game On":
                simplePlayer();
                backgroundMusic.play();
                startStatus(graphics);
                player.draw(graphics);
                drawMonsters(graphics);
                paitnWall(graphics);
                break;
            case "Game Over":
                count = 3;
                startStatus(graphics);
                showGameOver(graphics);
                break;
            case "Win":

                count = 3;
                startStatus(graphics);
                showWin(graphics);
                break;

        }
    }

    private void drawMonsters(Graphics graphics) {
        for (int i = 0; i < monster.length; i++) {
            monster[i].draw(graphics);
        }
    }

    private void init() {
        monster[0] = new Monster(3, 2);
        monster[1] = new Monster(21, 21);
        monster[2] = new Monster(20, 2);
        monster[3] = new Monster(4, 21);
        player = new Player(14, 11);
        life = 3;
    }

    private void startStatus(Graphics graphics) {

        for (int row = 0; row < maze.length; row++) {
            for (int col = 0; col < maze[0].length; col++) {
                Color color;

                switch (maze[row][col]) {
                    case 1:
                        graphics.drawImage(wall, col * 20, row * 20, this);
                        break;
                    case 2:
                        graphics.drawImage(small, col * 20, row * 20, null);
                        break;
                    default:
                        color = Color.WHITE;
                        graphics.setColor(color);
                        graphics.fillRect(20 * col, 20 * row, 20, 20);
                        graphics.setColor(color);
                        graphics.drawRect(20 * col, 20 * row, 20, 20);
                }

            }

        }


        for (int i = 0; i < life; i++) {
            graphics.drawImage(heart, maze[0].length * 20 - (i + 1) * 25, maze.length * 20 + 2, this);
        }

        String h = "Your health: " + life;
        Font small = new Font("Helvetica", Font.BOLD, 12);
        graphics.setFont(small);
        graphics.setColor(Color.RED);
        graphics.drawString(h, maze[0].length * 20 - (life + 1) * 25 - 55, maze.length * 20 + 15);

    }

    private void showStartScreen(Graphics graphics) {

        graphics.setColor(Color.BLACK);
        graphics.fillRect(0, 0, width, height);
        graphics.setColor(Color.white);
        graphics.drawRect(0, 0, width, height);

        String s = "MAZE GAME";
        String a = "PRESS ANY KEY";
        Font small = new Font("Helvetica", Font.BOLD, 30);


        graphics.setColor(Color.RED);
        graphics.setFont(small);
        graphics.drawString(s, width / 2 - 100, height / 2 - 100);
        Image batman = null;
        try {
            BufferedImage image = ImageIO.read(new File("src/image/batman.jpg"));

            batman = image.getScaledInstance(50, 60, image.SCALE_DEFAULT);

        } catch (IOException e) {
            e.printStackTrace();
        }
        graphics.drawImage(batman, width / 2 - 40, height / 2 - 30, null);
        graphics.drawString(a, width / 2 - 130, height / 2 + 100);

    }

    private void showIntro(Graphics graphics) {

        graphics.setColor(Color.BLACK);
        graphics.fillRect(0, 0, width, height);
        graphics.setColor(Color.white);
        graphics.drawRect(0, 0, width, height);

        Image book = null;
        Image bulb = null;
        Image warning = null;
        try {
            BufferedImage image = ImageIO.read(new File("src/image/book.png"));
            BufferedImage image1 = ImageIO.read(new File("src/image/bulb.png"));
            BufferedImage image2 = ImageIO.read(new File("src/image/warning.jpg"));
            BufferedImage image3=ImageIO.read(new File("src/image/road.jpg"));

            book = image.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
            bulb = image1.getScaledInstance(50, 50, Image.SCALE_AREA_AVERAGING);
            warning = image2.getScaledInstance(50, 50, Image.SCALE_AREA_AVERAGING);
            road=image3.getScaledInstance(20,20,Image.SCALE_AREA_AVERAGING);

        } catch (IOException e) {
            e.printStackTrace();
        }
        graphics.drawImage(book, 150, height / 2 - 100, null);
        graphics.drawImage(bulb, 150, height / 2 - 50, null);
        graphics.drawImage(warning, 150, height / 2, null);
        String s = "INTRODUCTIONS";
        String s1 = "Use the keyboard to control the batman.";
        String s2 = "Find a way through the maze and win the trophy!";
        String s3 = "Stay away from the monster! ";
        String s4 = "Each time you touch the monster you will loose health.";
        String p = "PRESS ANY KEY TO START";
        Font title = new Font("Helvetica", Font.BOLD, 30);
        graphics.setColor(Color.RED);
        graphics.setFont(title);
        graphics.drawString(s, width / 2 - 100, 100);
        graphics.drawString(p, width / 2 - 180, 520);
        Font title1 = new Font("Helvetica", Font.BOLD, 20);
        graphics.setColor(Color.RED);
        graphics.setFont(title1);
        graphics.drawString(s1, 210, height / 2 - 60);
        graphics.drawString(s2, 210, height / 2 - 10);
        graphics.drawString(s3, 210, height / 2 + 40);
        graphics.drawString(s4, 210, height / 2 + 70);

    }

    private void showGameOver(Graphics graphics) {

        String r = "Press R to restart";
        String l = "Game Over";
        Font small = new Font("Helvetica", Font.BOLD, 40);
        graphics.setColor(Color.RED);
        graphics.setFont(small);
        graphics.drawString(r, 100, 400);
        graphics.drawString(l, 130, 200);
    }

    private void showWin(Graphics graphics) {
        String s = "Congratulations";
        String a = "You win!";
        String r = "Press R to restart";
        Font small = new Font("Helvetica", Font.BOLD, 40);
        graphics.setColor(Color.RED);
        graphics.setFont(small);
        graphics.drawString(s, 130, 200);
        graphics.drawString(a, 190, 250);
        graphics.drawString(r, 100, 400);

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < monster.length; i++) {
            if (monster[i].getX() == player.getX() && monster[i].getY() == player.getY()) {
                life--;
                player = new Player(14, 11);
                if (life == 0) {
                    gameStatus = "Game Over";
                    backgroundMusic.stop();
                    init();
                }
            }
        }

        if (maze[player.getY()][player.getX()] == 2) {
            gameStatus = "Win";
            backgroundMusic.stop();
            init();
        }

        if (e.getSource() == timer) {
            for (int i = 0; i < monster.length; i++) {
                while (true) {
                    int randomNum = (int) (Math.random() * 5);
                    int position[];
                    monster[i].setDirection(Direction.values()[randomNum]);
                    position = predication.nextPosition(monster[i].getX(), monster[i].getY(), monster[i].getDirection());
                    if (maze[position[1]][position[0]] == 0) {
                        break;
                    }
                }
                monster[i].move();
                requestFocusInWindow();
                repaint();
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (count == 1) {
            gameStatus = "Introduction";
            count = 2;
            repaint();
        } else if (count == 2) {
            gameStatus = "Game On";
            timer.start();
            player.setDirection(Direction.None);
            control(e);
            player.move();
            repaint();
        }
        if (count == 3) {
            if (e.getKeyCode() == KeyEvent.VK_R) {
                gameStatus = "Game On";
            }
            if (gameStatus.equals("Game On")) {
                player.setDirection(Direction.None);
                control(e);
                player.move();
                repaint();

            }
        }
    }

    private void paitnWall(Graphics graphics) {


        for (int row = 0; row < maze.length; row++) {
            for (int col = 0; col < maze[0].length; col++) {
                if (((col - player.getX() > 3) || (player.getX() - col > 3)) || (row - player.getY() > 3 || player.getY() - row > 3)) {

                    Color color = Color.LIGHT_GRAY;
                    graphics.setColor(color);
                    graphics.fillRect(20 * col, 20 * row, 20, 20);
                    graphics.setColor(color);
                    graphics.drawRect(20 * col, 20 * row, 20, 20);
                }
            }
        }


    }

    private void control(KeyEvent e) {

        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                if (maze[player.getY() - 1][player.getX()] != 1) {
                    player.setDirection(Direction.Up);
                }
                break;
            case KeyEvent.VK_DOWN:
                if (maze[player.getY() + 1][player.getX()] != 1) {
                    player.setDirection(Direction.Down);
                }
                break;
            case KeyEvent.VK_LEFT:
                if (maze[player.getY()][player.getX() - 1] != 1) {
                    player.setDirection(Direction.Left);
                }
                break;
            case KeyEvent.VK_RIGHT:
                if (maze[player.getY()][player.getX() + 1] != 1) {
                    player.setDirection(Direction.Right);
                }
                break;
        }
    }



    @Override
    public void keyReleased(KeyEvent e) {



    }
}

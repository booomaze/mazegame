import javax.swing.*;
import java.awt.*;

/**
 * Created by qpen546 on 21/04/2017.
 */
public class Maze extends JFrame {
    public Maze(String title, int x, int y, int width, int height) {
        setTitle(title);
        setBounds(x, y, width, height);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Map panel = new Map(width,height);
        Container visibleArea = getContentPane();
        visibleArea.add(panel);
        pack();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Maze frame = new Maze("Maze game", 200, 200, 800, 600);
                frame.setVisible(true);
            }
        });
    }
}

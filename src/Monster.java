import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by qpen546 on 21/04/4017.
 */
public class Monster {
    private int x;
    private int y;
    private Direction direction;
    private Movement movement;
    Image small = null;

    public Monster(int x, int y) {
        this.x = x;
        this.y = y;

        try {
            BufferedImage image = ImageIO.read(new File("src/image/Monster.png"));

            small=image.getScaledInstance(20,20,image.SCALE_DEFAULT);

        } catch (IOException e) {
            e.printStackTrace();
        }

        movement = new Movement();
    }

    public int getX() {
        return x;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getY() {
        return y;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void move() {
        x = movement.nextPosition(x,y,direction)[0];
        y = movement.nextPosition(x,y,direction)[1];
    }

    public void draw(Graphics graphics) {

        graphics.drawImage(small,x * 20, y * 20,null);

    }
}

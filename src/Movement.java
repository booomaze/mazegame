/**
 * Created by qpen546 on 21/04/2017.
 */
public class Movement {


    public int[] nextPosition(int x,int y,Direction direction) {
        switch (direction) {
            case Up:
                y = y - 1;
                break;
            case Down:
                y = y + 1;
                break;
            case Left:
                x = x - 1;
                break;
            case Right:
                x = x + 1;
                break;
        }
        return new int[]{x,y};
    }
}

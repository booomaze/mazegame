import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by yzhb363 on 21/04/4017.
 */
public class Player {
    private int x;
    private int y;
    private Direction direction;
    private Movement movement = new Movement();
    Image small = null;

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        try {
            BufferedImage image = ImageIO.read(new File("src/image/batman.jpg"));

            small=image.getScaledInstance(20,20,image.SCALE_DEFAULT);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void move() {
        x = movement.nextPosition(x,y,direction)[0];
        y = movement.nextPosition(x,y,direction)[1];
    }

    public void draw(Graphics graphics) {

        graphics.drawImage(small,x * 20, y * 20,null);


    }

}
